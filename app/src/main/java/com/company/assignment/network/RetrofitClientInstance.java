package com.company.assignment.network;

import com.company.assignment.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Anjali on 07,July,2019
 * Class for Retrofit Instance, which is a networking library
 */
public class RetrofitClientInstance {
    private static Retrofit retrofit;

    /**
     * Function to get retrofit instance
     * @return retrofit instance
     */
    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
