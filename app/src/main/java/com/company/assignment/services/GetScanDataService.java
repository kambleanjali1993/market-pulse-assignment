package com.company.assignment.services;

import com.company.assignment.scans.model.ScanData;
import com.company.assignment.util.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Anjali on 07,July,2019
 * Service to get Data from server
 */
public interface GetScanDataService {

    /**
     * Method to get Scan Data from server
     * @return return array list of Scan Data in the form of ScanData model
     */
    @GET(Constants.PATH_SCAN_DATA)
    Call<ArrayList<ScanData>> getAllScanData();
}