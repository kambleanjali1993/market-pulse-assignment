package com.company.assignment.scans.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.assignment.R;
import com.company.assignment.criterias.CriteriaActivity;
import com.company.assignment.scans.model.ScanData;
import com.company.assignment.util.Constants;

import java.util.ArrayList;

/**
 * Created by Anjali on 07,July,2019
 * Adapter to assign scan values to list item
 */
public class ScanListAdapter extends RecyclerView.Adapter<ScanListAdapter.ViewHolder>{
    private ArrayList<ScanData> scanData;
    private Context context;

    public ScanListAdapter(ArrayList<ScanData> scanData, Context context){
        this.scanData = scanData;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.scan_list_item, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ScanData singleScanData = scanData.get(position);
        holder.textViewName.setText(singleScanData.getName());
        holder.textViewTag.setText(singleScanData.getTag());
        switch (singleScanData.getColor()){
            case Constants.GREEN:
                holder.textViewTag.setTextColor(context.getResources().getColor(R.color.colorGreen));
                break;
            case Constants.RED:
                holder.textViewTag.setTextColor(context.getResources().getColor(R.color.colorRed));
                break;
            default:
                holder.textViewTag.setTextColor(context.getResources().getColor(R.color.colorBlack));
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CriteriaActivity.class);
                intent.putExtra(Constants.SCAN_NAME,singleScanData.getName());
                intent.putExtra(Constants.SCAN_TAG,singleScanData.getTag());
                intent.putExtra(Constants.SCAN_COLOR,singleScanData.getColor());
                intent.putParcelableArrayListExtra(Constants.SCAN_CRITERIA,singleScanData.getCriteriaData());
                ((Activity) context).startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return scanData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewTag;
        ViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.text_view_name);
            this.textViewTag = (TextView) itemView.findViewById(R.id.text_view_tag);
        }
    }
}

