package com.company.assignment.scans;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.company.assignment.R;
import com.company.assignment.network.RetrofitClientInstance;
import com.company.assignment.scans.adapter.ScanListAdapter;
import com.company.assignment.scans.model.ScanData;
import com.company.assignment.services.GetScanDataService;
import com.company.assignment.util.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Anjali on 07,July,2019
 * Display list of scan data
 */
public class ScanListActivity extends AppCompatActivity {

    RecyclerView recyclerViewScanList;
    ProgressDialog progressDialog;
    ScanListAdapter scanListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerViewScanList = (RecyclerView) findViewById(R.id.recycler_view_scan_list);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getScanList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getScanList();
    }

    /**
     * Function to fetch scan data from server using GetScanDataService
     */
    public void getScanList() {
        if (Util.isNetworkConnected(ScanListActivity.this)) {
            progressDialog = new ProgressDialog(ScanListActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading_message));
            progressDialog.show();

            GetScanDataService getScanDataService = RetrofitClientInstance.getRetrofitInstance().create(GetScanDataService.class);
            Call<ArrayList<ScanData>> call = getScanDataService.getAllScanData();
            call.enqueue(new Callback<ArrayList<ScanData>>() {
                @Override
                public void onResponse(Call<ArrayList<ScanData>> call, Response<ArrayList<ScanData>> response) {
                    progressDialog.dismiss();
                    generateScanDataList(response.body());
                }

                @Override
                public void onFailure(Call<ArrayList<ScanData>> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ScanListActivity.this, getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Function to parse Scan Data, Criteria and Variable from server.
     * @param scanData
     */
    private void generateScanDataList(ArrayList<ScanData> scanData) {
        scanListAdapter = new ScanListAdapter(scanData,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ScanListActivity.this);
        recyclerViewScanList.setLayoutManager(layoutManager);
        recyclerViewScanList.setAdapter(scanListAdapter);
    }

    /**
     * press hardware back button twice within 2 seconds to exit the app
     */
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.exit_app), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}

