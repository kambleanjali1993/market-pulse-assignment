package com.company.assignment.scans.model;

import com.company.assignment.criterias.model.CriteriaData;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Anjali on 07,July,2019
 * Model class for Scan Data
 */
public class ScanData {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("tag")
    private String tag;
    @SerializedName("color")
    private String color;
    @SerializedName("criteria")
    private ArrayList<CriteriaData> criteriaData;

    public ScanData(int id, String name, String tag, String color, ArrayList<CriteriaData> criteriaData){
        this.id = id;
        this.name = name;
        this.tag = tag;
        this.color = color;
        this.criteriaData = criteriaData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public ArrayList<CriteriaData> getCriteriaData() {
        return criteriaData;
    }

    public void setCriteriaData(ArrayList<CriteriaData> criteriaData) {
        this.criteriaData = criteriaData;
    }
}

