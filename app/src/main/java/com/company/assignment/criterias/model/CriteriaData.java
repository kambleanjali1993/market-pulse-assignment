package com.company.assignment.criterias.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.company.assignment.variables.model.VariableData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Anjali on 07,July,2019
 * Model class for ScanData's Criteria
 */
public class CriteriaData implements Parcelable, Serializable {
    @SerializedName("type")
    private String type;
    @SerializedName("text")
    private String text;
    @SerializedName("variable")
    private HashMap<String, VariableData> variableData;

    public CriteriaData(String type, String text,  HashMap<String,VariableData> variableData){
        this.type = type;
        this.text = text;
        this.variableData = variableData;
    }

    protected CriteriaData(Parcel in) {
        type = in.readString();
        text = in.readString();
        variableData = (HashMap<String, VariableData>) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(text);
        dest.writeSerializable(variableData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CriteriaData> CREATOR = new Creator<CriteriaData>() {
        @Override
        public CriteriaData createFromParcel(Parcel in) {
            return new CriteriaData(in);
        }

        @Override
        public CriteriaData[] newArray(int size) {
            return new CriteriaData[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public HashMap<String, VariableData> getVariableData() {
        return variableData;
    }

    public void setVariableData(HashMap<String, VariableData> variableData) {
        this.variableData = variableData;
    }
}
