package com.company.assignment.criterias.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.assignment.R;
import com.company.assignment.criterias.model.CriteriaData;
import com.company.assignment.util.Constants;
import com.company.assignment.util.Util;
import com.company.assignment.variables.model.VariableData;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Anjali on 07,July,2019
 */
public class CriteriaListAdapter extends RecyclerView.Adapter<CriteriaListAdapter.ViewHolder>{
    private ArrayList<CriteriaData> criteriaData;
    private Context context;

    public CriteriaListAdapter(ArrayList<CriteriaData> criteriaData, Context context){
        this.criteriaData = criteriaData;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.criteria_list_item, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CriteriaData singleCriteriaData = criteriaData.get(position);

        if(position == getItemCount()-1){
            //if last position hide "and"
            holder.textViewAnd.setVisibility(View.GONE);
        }

        switch (singleCriteriaData.getType()) {
            case Constants.TYPE_PLAIN_TEXT:
                holder.textViewName.setText(singleCriteriaData.getText());
                break;
            case Constants.TYPE_VARIABLE:
                Set<String> variableKey = singleCriteriaData.getVariableData().keySet();
                String textToShow = singleCriteriaData.getText();
                ArrayList<ClickableSpan> clickableSpans = new ArrayList<>();
                ArrayList<String> clickableValues = new ArrayList<>();
                for(String key : variableKey){
                    if(singleCriteriaData.getText().contains(key)){
                        final VariableData variableData = singleCriteriaData.getVariableData().get(key);
                        String newString = "";
                        switch (variableData.getType()) {
                            case Constants.TYPE_VALUE:
                                newString = Util.nf.format(variableData.getValues().get(0));

                                break;
                            case Constants.TYPE_INDICATOR:
                                newString = Util.nf.format(variableData.getDefaultValue());
                                break;
                            default:
                                break;
                        }

                        newString = Constants.OPENING_BRACKET+newString+Constants.CLOSING_BRACKET;
                        ClickableSpan clickableSpan = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {

                            }
                        };
                        clickableSpans.add(clickableSpan);
                        clickableValues.add(newString);
                        textToShow = textToShow.replace(key, newString);
                    }
                }
                SpannableString spannableString = new SpannableString(textToShow);
                for (int i = 0; i < clickableValues.size(); i++) {
                    ClickableSpan clickableSpan = clickableSpans.get(i);
                    String link = clickableValues.get(i);

                    int startIndexOfLink = textToShow.indexOf(link);
                    spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                holder.textViewName.setHighlightColor(Color.TRANSPARENT);
                holder.textViewName.setMovementMethod(LinkMovementMethod.getInstance());
                holder.textViewName.setText(spannableString, TextView.BufferType.SPANNABLE);
                break;
            default:
                holder.textViewName.setText(singleCriteriaData.getText());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return criteriaData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewAnd;
        ViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.text_view_name);
            this.textViewAnd = (TextView) itemView.findViewById(R.id.text_view_and);
        }
    }
}

