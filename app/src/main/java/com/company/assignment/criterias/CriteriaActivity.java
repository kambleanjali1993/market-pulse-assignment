package com.company.assignment.criterias;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.company.assignment.R;
import com.company.assignment.criterias.adapter.CriteriaListAdapter;
import com.company.assignment.criterias.model.CriteriaData;
import com.company.assignment.util.Constants;

import java.util.ArrayList;

/**
 * Created by Anjali on 07,July,2019
 */
public class CriteriaActivity  extends AppCompatActivity {
    RecyclerView recyclerViewScanList;
    CriteriaListAdapter criteriaListAdapter;
    TextView textViewName;
    TextView textViewTag;
    ArrayList<CriteriaData> scanCriteria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criteria);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerViewScanList = (RecyclerView) findViewById(R.id.recycler_view_criteria_list);
        textViewName = (TextView) findViewById(R.id.text_view_name);
        textViewTag = (TextView) findViewById(R.id.text_view_tag);
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getData(){
        if(getIntent()!=null){
            textViewName.setText(getIntent().getStringExtra(Constants.SCAN_NAME));
            textViewTag.setText(getIntent().getStringExtra(Constants.SCAN_TAG));
            switch (getIntent().getStringExtra(Constants.SCAN_COLOR)){
                case Constants.GREEN:
                    textViewTag.setTextColor(this.getResources().getColor(R.color.colorGreen));
                    break;
                case Constants.RED:
                    textViewTag.setTextColor(this.getResources().getColor(R.color.colorRed));
                    break;
                default:
                    textViewTag.setTextColor(this.getResources().getColor(R.color.colorBlack));
                    break;
            }
            scanCriteria = getIntent().getParcelableArrayListExtra(Constants.SCAN_CRITERIA);
            criteriaListAdapter = new CriteriaListAdapter(scanCriteria,this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CriteriaActivity.this);
            recyclerViewScanList.setLayoutManager(layoutManager);
            recyclerViewScanList.setAdapter(criteriaListAdapter);
        }
    }

}

