package com.company.assignment.variables.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Anjali on 07,July,2019
 * Model class for criteria's variable
 */
public class VariableData implements Parcelable, Serializable {
    @SerializedName("type")
    private String type;
    @SerializedName("study_type")
    private String studyType;
    @SerializedName("parameter_name")
    private String parameterName;
    @SerializedName("min_value")
    private int minValue;
    @SerializedName("max_value")
    private int maxValue;
    @SerializedName("default_value")
    private int defaultValue;
    @SerializedName("values")
    private ArrayList<Float> values;

    public VariableData(String type, String studyType, String parameterName, int minValue, int maxValue, int defaultValue, ArrayList<Float> values){
        this.type = type;
        this.studyType = studyType;
        this.parameterName = parameterName;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.defaultValue = defaultValue;
        this.values = values;
    }

    protected VariableData(Parcel in) {
        type = in.readString();
        studyType = in.readString();
        parameterName = in.readString();
        minValue = in.readInt();
        maxValue = in.readInt();
        defaultValue = in.readInt();
        values = (ArrayList<Float>) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(studyType);
        dest.writeString(parameterName);
        dest.writeInt(minValue);
        dest.writeInt(maxValue);
        dest.writeInt(defaultValue);
        dest.writeSerializable(values);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VariableData> CREATOR = new Creator<VariableData>() {
        @Override
        public VariableData createFromParcel(Parcel in) {
            return new VariableData(in);
        }

        @Override
        public VariableData[] newArray(int size) {
            return new VariableData[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }

    public ArrayList<Float> getValues() {
        return values;
    }

    public void setValues(ArrayList<Float> values) {
        this.values = values;
    }
}
