package com.company.assignment.variables.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.assignment.R;
import com.company.assignment.util.Util;

import java.util.ArrayList;

/**
 * Created by Anjali on 07,July,2019
 */
public class VariableListAdapter extends RecyclerView.Adapter<VariableListAdapter.ViewHolder>{
    private ArrayList<Float> variableValues;
    private Context context;

    public VariableListAdapter(ArrayList<Float> variableValues, Context context ){
        this.variableValues = variableValues;
        this.context = context;
    }

    @Override
    public VariableListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.variable_value_list_item, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(VariableListAdapter.ViewHolder holder, int position) {
        final Float variableValue = variableValues.get(position);
        holder.textViewValue.setText(Util.nf.format(variableValue));
    }

    @Override
    public int getItemCount() {
        return variableValues.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewValue;
        ViewHolder(View itemView) {
            super(itemView);
            this.textViewValue = (TextView) itemView.findViewById(R.id.text_view_value);
        }
    }
}

