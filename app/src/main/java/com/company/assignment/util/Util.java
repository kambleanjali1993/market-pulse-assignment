package com.company.assignment.util;

import android.content.Context;
import android.net.ConnectivityManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Anjali on 07,July,2019
 * class for common methods
 */
public class Util {

    /**
     * Number format for float to decimal
     */
    public static final NumberFormat nf = new DecimalFormat("#.####");

    /**
     * Check Internet connection
     * @param context Current context of application
     * @return if internet is available return true else return false
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        assert connectivityManager != null;
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}

