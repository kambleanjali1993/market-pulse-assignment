package com.company.assignment.util;

/**
 * Created by Anjali on 07,July,2019
 */
public class Constants {
    //Networking constants
    public static final String BASE_URL = "https://mp-android-challenge.herokuapp.com";
    public static final String PATH_SCAN_DATA = "/data";

    public static final String GREEN = "green";
    public static final String RED = "red";
    public static final String TYPE_PLAIN_TEXT = "plain_text";
    public static final String TYPE_VARIABLE = "variable";
    public static final String TYPE_VALUE = "value";
    public static final String TYPE_INDICATOR = "indicator";
    public static final String OPENING_BRACKET = "(";
    public static final String CLOSING_BRACKET = ")";

    //Constants required for Criteria Activity
    public static final String SCAN_NAME = "scan_name";
    public static final String SCAN_TAG = "scan_tag";
    public static final String SCAN_COLOR = "scan_color";
    public static final String SCAN_CRITERIA = "scan_criteria";
}
